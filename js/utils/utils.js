'use strict';

/* Global Utils */

// Extending String prototype yo use this method globally 
// Method to interpolate string 
// Ex. 	code   -> "Hello, {str}  ".supplant({ str: 'World' });
// 		output ->  Hello, World
String.prototype.supplant = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
};


window.utils = {
	/**
	 * Method to prepend element to a container
	 * @param  {[DOM Element]} parentEle Container element
	 * @param  {[DOM Element]} newEle    Element to prepend
	 */
	prependChild: function(parentEle, newEle) {
		parentEle.insertBefore(newEle, parentEle.firstChild);
	},
	ajax: function(method, url, body, successCb){
		var xmlHttp = new XMLHttpRequest();
	    xmlHttp.onreadystatechange = function() { 
	        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
	            successCb(xmlHttp.responseText);
	        }
	    }
	    xmlHttp.open(method, url, true);
	    xmlHttp.send(body || null);
	},
	/**
	 * Returns Promise for ajax class
	 * @param  {[String]} 			method GET, POST stc.
	 * @param  {[String]} 			url    url
	 * @param  {[Object, String]} 	body   body object
	 * @return {[Promise]}        	promise
	 */
	ajaxPromise: function(method, url, body){
		var self = this;
		return new Promise(function(resolve, reject){
			self.ajax(method, url, body, resolve, reject);
		});
	},
	/**
	 * Function to convert Base64 image data to 
	 * @param  {[type]}   imageBase64 	base64 Image Source
	 * @param  {Function} cb       		callback function
	 * @return {[Array]}              	array of pixels
	 */
	base64ToPixels: function(imageBase64, cb){
		var canvasEle = document.createElement("canvas");
		var ctx = canvasEle.getContext("2d");
		var img = new Image();
	    img.src = imageBase64;
	    img.onload = function(){
	    	canvasEle.setAttribute('height', img.height);
	    	canvasEle.setAttribute('width', img.width);
	    	ctx.drawImage(img, 0, 0);
	    	cb && cb({
	    		pixels: ctx.getImageData(0, 0, img.width, img.height).data,
	    		height: img.height,
	    		width: img.width
	    	});
		};
	}
}
