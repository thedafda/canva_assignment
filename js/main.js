'use strict';

(function(win, UI, utils){

	var worker = new Worker('js/worker/mosaic_engine.js');

	// Document ready
	document.addEventListener("DOMContentLoaded", function(){

		var imagefileinput, mosaicpreviewer;

		// FileInput UI component
	    imagefileinput = new UI.ImageFileInput({
	    	onFileSelect: function(imageBase64){
	    		utils.base64ToPixels(imageBase64, function(o){
	    			var job = {
						imageData: o.pixels,
						imgDimensions: {
							height: o.height,
							width: o.width
						},
						gridDimensions: {
							height: TILE_HEIGHT,
							width: TILE_WIDTH
						}
					};
	    			worker.postMessage(job);
	    		});

	    	}
	    });

	    // MosaicPreviewer UI component
	    mosaicpreviewer = new UI.MosaicPreviewer();

	    worker.addEventListener('message', function(e) {
	    	mosaicpreviewer.appendMoisaicGrid(e.data);
	    });

	    document.body.appendChild(imagefileinput.ele);
	    document.body.appendChild(mosaicpreviewer.ele);
	});

})(window, window.UI, window.utils)

