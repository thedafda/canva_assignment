'use strict';

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

/**
 * Method to convert RBD to HEX color
 * @param  {[Number]} r Red Color
 * @param  {[Number]} g Green Color
 * @param  {[Number]} b Blue Color
 * @return {[String]}  Hex Code  
 */
function rgbToHex(r, g, b) {
    return componentToHex(r) + componentToHex(g) + componentToHex(b);
}

/**
 * Method to get pixles from image data 
 * @param  {[Array]}  pixels    pixels array
 * @param  {[Number]} canvasX   x-coordinate
 * @param  {[Number]} canvasY   y-coordinate
 * @param  {[Number]} imgHeight Image Height
 * @param  {[Number]} imgWidth  Image Width
 * @return {[Array]}            Array of RBG color
 */
function getPixelFromArray(pixels, x, y, imgHeight, imgWidth) {
    var pixelRedIndex = ((y - 1) * (imgWidth * 4)) + ((x - 1) * 4);
    return [ pixels[pixelRedIndex], pixels[pixelRedIndex + 1] , pixels[pixelRedIndex + 2], pixels[pixelRedIndex + 3]];
}

/**
 * Method to generate mosaic grid from pixel data
 * @param  {[Array]}  imgData    Pixles
 * @param  {[Number]} imgHeight  Image Height
 * @param  {[Number]} imgWidth   Image Width
 * @param  {[Number]} gridHeight Mosaic Tile Height
 * @param  {[Number]} gridWidth  mosaic Tile Width
 * @return {[Array]}           
 */
function getGrid(imgData, imgHeight, imgWidth, gridHeight, gridWidth){
    var i, j, p, q;
    var xi = Math.ceil(imgWidth / gridWidth);
    var yi = Math.ceil(imgHeight / gridHeight);
    var tempArr = [];

    for(i = 0; i < yi; i++){
        tempArr[i] = [];
        for(j = 0; j < xi; j++){
            var avrgColor;
            var nool = [];
            var loo = i * gridWidth;
            var poo = j * gridHeight;
            var pooAvrg = [0, 0, 0];
            for(q = loo; q < loo + gridWidth; q++){
                for(p = poo; p < poo + gridHeight; p++){
                    if(q < imgHeight && p < imgWidth){
                        var ppp = getPixelFromArray(imgData, p, q, imgHeight, imgWidth);
                        nool.push(ppp);
                        pooAvrg[0] = pooAvrg[0] + ppp[0];
                        pooAvrg[1] = pooAvrg[1] + ppp[1];
                        pooAvrg[2] = pooAvrg[2] + ppp[2];
                    }
                }
            }
            pooAvrg[0] = Math.ceil(pooAvrg[0] / nool.length);
            pooAvrg[1] = Math.ceil(pooAvrg[1] / nool.length);
            pooAvrg[2] = Math.ceil(pooAvrg[2] / nool.length);
            tempArr[i][j] = rgbToHex(pooAvrg[0], pooAvrg[1], pooAvrg[2]);
        }
    }
    return tempArr;
}

var defaultDimensions = { height: 16, width: 16 };

self.addEventListener('message', function(e) {
    var data = e.data;
    // Creatting job object
    var job = {
        timestamp: (new Date()).getTime(),
        imageData: data.imageData,
        imgDimensions: data.imgDimensions,
        gridDimensions: data.gridDimensions || defaultDimensions
    };

    // getting mosaic grid
    var grid = getGrid(
        job.imageData,
        job.imgDimensions.height,
        job.imgDimensions.width,
        job.gridDimensions.height,
        job.gridDimensions.width
    )
    
    job.grid = grid;
    self.postMessage(job);

}, false);

