(function(win){
	'use strict';

	/**
	 * Image File Read Component 
	 * Providers Choose or Drang and Drop functionality
	 */

	function ImageFileInput(options){
		this.opt = options || {};
		this.ele = document.createElement('div');
		this.ele.innerHTML = this.template;
		this.img = this.ele.querySelector('img');
		this.input = this.ele.querySelector('input');
		this.bindEvents();
	}

	ImageFileInput.prototype.template = [
		"<div class='file-input'>",
			"<img/>",
			"<h2> Click or Drag-n-drop Images </h2>",
			"<div><input type='file' /></div>",
		"</div>"
	].join('');

	ImageFileInput.prototype.bindEvents = function(){
		var self = this;
		this.handlers = {
			dragover: function(e){
				e.preventDefault();
				e.stopPropagation();
			},
			dragleave: function(e){
				e.preventDefault();
				e.stopPropagation();
			},
			drop: function(e){
				e.preventDefault();
				var file = e.dataTransfer.files && e.dataTransfer.files[0];
				self.updateImage(file);
				return false;
			},
			click: function(){
				self.input.click();
			},
			change: function(e){
				var file = e.target.files && e.target.files[0];
				self.updateImage(file);
			}
		}
		this.ele.addEventListener("dragover", this.handlers.dragover);
		this.ele.addEventListener("dragleave", this.handlers.dragleave);
		this.ele.addEventListener("drop", this.handlers.drop);
		this.ele.addEventListener("click", this.handlers.click);
		this.input.addEventListener("change", this.handlers.change);
	};

	ImageFileInput.prototype.updateImage = function (file, cb){
		var self = this;
		if(file){
			self.readFile(file, function(o){
				self.img.src = o;
				self.opt.onFileSelect && self.opt.onFileSelect(o, {
					imgEle: self.img
				});
			});
		}
	};	

	ImageFileInput.prototype.readFile = function (file, cb){
		var reader = new FileReader();
		reader.onload = function(o){
			cb && cb(o.target.result);
		};
		reader.readAsDataURL(file);
	};

	ImageFileInput.prototype.unbindEvents = function(){
		this.ele.removeEventListener("dragover", this.handlers.dragover);
		this.ele.removeEventListener("dragleave", this.handlers.dragleave);
		this.ele.removeEventListener("drop", this.handlers.drop);
		this.ele.removeEventListener("click", this.handlers.click);
		this.input.addEventListener("change", this.handlers.change);
	};

	ImageFileInput.prototype.destory = function() {
		this.unbindEvents();
		this.ele.remove();
	};

	win.UI = win.UI || {};
	win.UI.ImageFileInput = ImageFileInput;

})(window);